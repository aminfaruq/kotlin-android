package co.id.footballmania.network

import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

class RetrofitAppTest{
    @Test
    fun testRetrofit(){
        val apiClient = mock(RetrofitApp.create()::class.java)
        val  url = "https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php?l=English%20Premier%20League"
        apiClient.listClub(url)
        verify(apiClient).listClub(url)
    }
}