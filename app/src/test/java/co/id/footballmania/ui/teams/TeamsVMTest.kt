package co.id.footballmania.ui.teams

import androidx.lifecycle.*
import co.id.footballmania.model.listclub.TeamsItem
import co.id.footballmania.network.RetrofitApp
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class TeamsVMTest {

    @Mock
    private val apiClient = RetrofitApp.create()

    @Mock
    private val teamsFragment = TeamsFragment()

    private lateinit var teamsVM: TeamsVM

    @Mock
    var observer: Observer<MutableList<TeamsItem>>? = null

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        teamsVM = TeamsVM()
        observer?.let { teamsVM.getTeams().observeForever(it) }
    }

    @Test
    fun getTeams() {
        val league = "English Premiere League"

    }
}


