package co.id.footballmania.utils

object Constant {
    const val ID_LEAGUE = "id_league"
    const val ID_EVENT = "id_event"
    const val ID_HOME_TEAMS = "id_home_teams"
    const val ID_AWAY_TEAMS = "id_away_teams"
}