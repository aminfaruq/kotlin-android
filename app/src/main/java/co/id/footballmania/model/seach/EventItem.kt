package co.id.footballmania.model.seach

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import retrofit2.http.Field

@Parcelize
data class EventItem(

	@field:SerializedName("strSport")
	val strSport: String? = null,

	@field:SerializedName("idLeague")
	val idLeague: String? = null,

	@field:SerializedName("idEvent")
	val idEvent: String? = null,

	@field:SerializedName("intRound")
	val intRound: String? = null,

	@field:SerializedName("idHomeTeam")
	val idHomeTeam: String? = null,

	@field:SerializedName("idAwayTeam")
	val idAwayTeams : String? = null,

	@field:SerializedName("intHomeScore")
	val intHomeScore: String? = null,

	@field:SerializedName("dateEvent")
	val dateEvent: String? = null,

	@field:SerializedName("strAwayTeam")
	val strAwayTeam: String? = null,

	@field:SerializedName("strFilename")
	val strFilename: String? = null,

	@field:SerializedName("strTime")
	val strTime: String? = null,

	@field:SerializedName("strLocked")
	val strLocked: String? = null,

	@field:SerializedName("strSeason")
	val strSeason: String? = null,

	@field:SerializedName("strEventAlternate")
	val strEventAlternate: String? = null,

	@field:SerializedName("strEvent")
	val strEvent: String? = null,

	@field:SerializedName("strHomeTeam")
	val strHomeTeam: String? = null,

	@field:SerializedName("strLeague")
	val strLeague: String? = null,

	@field:SerializedName("intAwayScore")
	val intAwayScore: String? = null

): Parcelable{

	data class ResponseSearch(

		@field:SerializedName("event")
		val event: List<EventItem>? = null,

		@field:SerializedName("events")
    	val events: List<EventItem>? = null


	)
}