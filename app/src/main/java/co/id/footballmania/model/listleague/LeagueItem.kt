package co.id.footballmania.model.listleague

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LeagueItem (
    val id : String?,
    val leagueName: String?,
    val leagueImage: String?
): Parcelable