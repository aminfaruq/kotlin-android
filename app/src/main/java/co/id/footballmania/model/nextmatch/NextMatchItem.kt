package co.id.footballmania.model.nextmatch

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NextMatchItem(

	@field:SerializedName("strSport")
	val strSport: String? = null,

	@field:SerializedName("idLeague")
	val idLeague: String? = null,

	@field:SerializedName("idEvent")
	val idEvent: String? = null,

	@field:SerializedName("intRound")
	val intRound: String? = null,

	@field:SerializedName("idHomeTeam")
	val idHomeTeam: String? = null,

	@field:SerializedName("intHomeScore")
	val intHomeScore: String? = null,

	@field:SerializedName("dateEvent")
	val dateEvent: String? = null,


	@field:SerializedName("strAwayTeam")
	val strAwayTeam: String? = null,

	@field:SerializedName("idAwayTeam")
	val idAwayTeam: String? = null,

	@field:SerializedName("dateEventLocal")
	val dateEventLocal: String? = null,

	@field:SerializedName("strFilename")
	val strFilename: String? = null,

	@field:SerializedName("strTime")
	val strTime: String? = null,

	@field:SerializedName("strTimeLocal")
	val strTimeLocal: String? = null,

	@field:SerializedName("strLocked")
	val strLocked: String? = null,

	@field:SerializedName("strSeason")
	val strSeason: String? = null,

	@field:SerializedName("strEventAlternate")
	val strEventAlternate: String? = null,

	@field:SerializedName("strEvent")
	val strEvent: String? = null,

	@field:SerializedName("strHomeTeam")
	val strHomeTeam: String? = null,

	@field:SerializedName("strLeague")
	val strLeague: String? = null,

	@field:SerializedName("intAwayScore")
	val intAwayScore: String? = null

):Parcelable{
	data class ResponseNextMatch(
		@field:SerializedName("events")
		val events: MutableList<NextMatchItem>? = null
	)
}