package co.id.footballmania.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class MyDatabaseOpenHelper(ctx: Context) :
    ManagedSQLiteOpenHelper(ctx, "FavoriteTeam.db", null, 1) {

    companion object {
        private var instance: MyDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
            if (instance == null) {
                instance = MyDatabaseOpenHelper(ctx.applicationContext)
            }
            return instance as MyDatabaseOpenHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        //Here you create tables
        db?.createTable(
            Favorite.TABLE_FAVORITE, true,
            Favorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            Favorite.EVENT_ID to TEXT ,
            Favorite.HOME_ID to TEXT,
            Favorite.AWAY_ID to TEXT,
            Favorite.STR_HOME_TEAM to TEXT,
            Favorite.STR_AWAY_TEAM to TEXT,
            Favorite.INT_HOME_SCORE to TEXT,
            Favorite.INT_AWAY_SCORE to TEXT,
            Favorite.STR_HOME_RED_CARDS to TEXT,
            Favorite.STR_AWAY_RED_CARDS to TEXT,
            Favorite.STR_HOME_YELLOW_CARDS to TEXT,
            Favorite.STR_AWAY_YELLOW_CARDS to TEXT,
            Favorite.INT_HOME_SHOTS to TEXT,
            Favorite.INT_AWAY_SHOTS to TEXT,
            Favorite.DATE_EVENT to TEXT,
            Favorite.STR_HOME_FORMATION to TEXT,
            Favorite.STR_AWAY_FORMATION to TEXT,
            Favorite.STR_BADGE_HOME to TEXT,
            Favorite.STR_BADGE_AWAY to TEXT,
            Favorite.STR_EVENT to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        // Here you can upgrade tables, as usual
        db?.dropTable(Favorite.TABLE_FAVORITE, true)
    }
}

// Access property for Context
val Context.database: MyDatabaseOpenHelper
    get() = MyDatabaseOpenHelper.getInstance(applicationContext)