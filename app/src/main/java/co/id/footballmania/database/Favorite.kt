package co.id.footballmania.database

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Favorite (
    val id : Long?,
    val eventId : String?,
    val homeId : String,
    val awayId : String,
    val strHomeTeam : String?,
    val strAwayTeam : String?,
    val intHomeScore : String?,
    val intAwayScore : String?,
    val strHomeRedCards : String?,
    val strAwayRedCards : String?,
    val strHomeYellowCards : String?,
    val strAwayYellowCards : String?,
    val intHomeShots : String?,
    val intAwayShots : String?,
    val dateEvent : String?,
    val strHomeFormation : String?,
    val strAwayFormation : String?,
    val strBadgeHome : String?,
    val strBadgeAway : String?,
    val strEvent : String
) : Parcelable {
    companion object {

        const val TABLE_FAVORITE: String = "TABLE_FAVORITE"
        const val ID : String = "ID_"
        const val EVENT_ID: String = "EVENT_ID"
        const val HOME_ID : String = "HOME_ID"
        const val AWAY_ID : String = "AWAY_ID"
        const val STR_HOME_TEAM: String = "STR_HOME_TEAM"
        const val STR_AWAY_TEAM: String = "STR_AWAY_TEAM"
        const val INT_HOME_SCORE: String = "INT_HOME_SCORE"
        const val INT_AWAY_SCORE: String = "INT_AWAY_SCORE"
        const val STR_HOME_RED_CARDS: String = "STR_HOME_RED_CARDS"
        const val STR_AWAY_RED_CARDS: String = "STR_AWAY_RED_CARDS"
        const val STR_HOME_YELLOW_CARDS: String = "STR_HOME_YELLOW_CARDS"
        const val STR_AWAY_YELLOW_CARDS: String = "STR_AWAY_YELLOW_CARDS"
        const val INT_HOME_SHOTS : String = "INT_HOME_SHOTS"
        const val INT_AWAY_SHOTS : String = "INT_AWAY_SHOTS"
        const val DATE_EVENT : String = "DATE_EVENT"
        const val STR_HOME_FORMATION : String = "STR_HOME_FORMATION"
        const val STR_AWAY_FORMATION : String = "STR_AWAY_FORMATION"
        const val STR_BADGE_HOME : String = "STR_BADGE_HOME"
        const val STR_BADGE_AWAY : String = "STR_BADGE_AWAY"
        const val STR_EVENT : String = "STR_EVENT"
    }
}