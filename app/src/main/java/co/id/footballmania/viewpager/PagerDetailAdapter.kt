package co.id.footballmania.viewpager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import co.id.footballmania.ui.detailleague.favoritematch.FavoriteMatchFragment
import co.id.footballmania.ui.detailleague.nextmatch.NextMatch
import co.id.footballmania.ui.detailleague.previousmatch.PreviousMatch

@Suppress("DEPRECATION")
class PagerDetailAdapter (fm: FragmentManager, idLeague : String): FragmentPagerAdapter(fm) {

    // sebuah list yang menampung objek Fragment
    private val pages = listOf(
        NextMatch.newInstance(idLeague),
        PreviousMatch.newInstance(idLeague),
        FavoriteMatchFragment()
    )

    // menentukan fragment yang akan dibuka pada posisi tertentu
    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }

    // judul untuk tabs
    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Next Match"
            1 -> "Previous Match"
            else -> "Favorite Match"
        }
    }
}