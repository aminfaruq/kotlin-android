package co.id.footballmania.network

import co.id.footballmania.model.detailleague.DetailLeagueModel
import co.id.footballmania.model.detailmatch.DetailMatch
import co.id.footballmania.model.listclub.TeamsItem
import co.id.footballmania.model.seach.EventItem
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface SportService {
    //detail league
    @GET("api/v1/json/1/lookupleague.php")
    fun detailLeague(
        @Query("id")id : String
    ): Call<DetailLeagueModel.ResponseDetailLeague>

    //list club
    @GET("api/v1/json/1/search_all_teams.php")
    fun listClub(
        @Query("l") league: String
    ): Call<TeamsItem.ResponseTeams>

    //search
    @GET("api/v1/json/1/searchevents.php")
    fun searchEvent(
        @Query("e")event : String
    ):Call<EventItem.ResponseSearch>

    //nextmatch
    @GET("api/v1/json/1/eventsnextleague.php")
    fun nextMatch(
        @Query("id") idLeague : String
    ): Call<EventItem.ResponseSearch>

    //previous
    @GET("api/v1/json/1/eventspastleague.php")
    fun previousMatch(
        @Query("id") idLeague: String
    ): Call<EventItem.ResponseSearch>

    //detail match
    @GET("api/v1/json/1/lookupevent.php")
    fun detailMatch(
        @Query("id") idEvent : String
    ): Call<DetailMatch.ResponseMatch>

    //lookup teams
    @GET("api/v1/json/1/lookupteam.php")
    fun lookupTeams(
        @Query("id")idTeams : String
    ): Call<TeamsItem.ResponseTeams>

}