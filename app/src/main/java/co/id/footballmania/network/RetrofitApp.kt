package co.id.footballmania.network

import co.id.footballmania.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitApp {

    fun create(): SportService {
        val client = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return client.create(SportService::class.java)
    }
}