package co.id.footballmania.ui.detailleague.favoritematch


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import co.id.footballmania.R
import co.id.footballmania.database.Favorite
import co.id.footballmania.database.database
import co.id.footballmania.ui.detailleague.detailmatch.DetailMatchActivity
import co.id.footballmania.utils.Constant
import kotlinx.android.synthetic.main.fragment_favorite_match.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.startActivity

/**
 * A simple [Fragment] subclass.
 */
class FavoriteMatchFragment : Fragment() {
    private var favorites: MutableList<Favorite> = mutableListOf()
    private lateinit var adapter: FavoriteMatchAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite_match, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapter = FavoriteMatchAdapter(favorites){
            val intent = Intent(context, DetailMatchActivity::class.java)
            intent.putExtra(Constant.ID_EVENT , it.eventId)
            intent.putExtra(Constant.ID_HOME_TEAMS, it.homeId)
            intent.putExtra(Constant.ID_AWAY_TEAMS, it.awayId)
            startActivity(intent)
        }
        rv_favorite.adapter = adapter
        rv_favorite.layoutManager = LinearLayoutManager(context)
        showFavorite()
    }

    override fun onResume() {
        super.onResume()
        showFavorite()
    }

    private fun showFavorite(){
        favorites.clear()
        context?.database?.use {
            val result = select(Favorite.TABLE_FAVORITE)
            val favorite = result.parseList(classParser<Favorite>())
            favorites.addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }

}


