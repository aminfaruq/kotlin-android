package co.id.footballmania.ui.detailleague.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.id.footballmania.R
import co.id.footballmania.model.seach.EventItem
import co.id.footballmania.utils.DateTime
import kotlinx.android.synthetic.main.item_detail.view.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class SearchAdapter(private val onItemClick: OnItemClick) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    private val match = ArrayList<EventItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val mView = LayoutInflater.from(parent.context).inflate(R.layout.item_detail,parent,false)
        return ViewHolder(mView,onItemClick)
    }

    override fun getItemCount(): Int = match.size

    fun setData(item: ArrayList<EventItem>){
        match.clear()
        match.addAll(item)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(match[position])
    }

    class ViewHolder(val view: View, private val onItemClick: OnItemClick) : RecyclerView.ViewHolder(view) {
        fun bindItem(teams : EventItem ){
            with(itemView){
                tv_date.text = DateTime.getLongDate(teams.dateEvent ?: "")
                tv_score_home.text = teams.intHomeScore
                tv_score_away.text = teams.intAwayScore
                imgMatch1.text = teams.strHomeTeam
                imgMatch2.text = teams.strAwayTeam
                view.onClick {
                    onItemClick.onClick(teams)
                }
            }
        }
    }

    interface OnItemClick{
        fun onClick(item : EventItem)
    }

}