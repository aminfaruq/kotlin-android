package co.id.footballmania.ui.teams

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import co.id.footballmania.R
import co.id.footballmania.model.listclub.TeamsItem
import co.id.footballmania.utils.loadImageUrl
import kotlinx.android.synthetic.main.item_league.view.*

class TeamsAdapter : RecyclerView.Adapter<TeamsAdapter.ViewHolder>() {
    private val mData = ArrayList<TeamsItem>()

    fun setData(item: ArrayList<TeamsItem>){
        mData.clear()
        mData.addAll(item)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val mView = LayoutInflater.from(parent.context).inflate(R.layout.item_league,parent,false)
        return ViewHolder(mView)
    }

    override fun getItemCount(): Int = mData.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mData[position])
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(teamItem : TeamsItem){
            with(itemView){
                tv_name.text = teamItem.strTeam
                teamItem.strTeamBadge?.let { imgLeague.loadImageUrl(it) }
                view.setOnClickListener {
                    Toast.makeText(context, "You choose ${teamItem.strTeam}",Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}