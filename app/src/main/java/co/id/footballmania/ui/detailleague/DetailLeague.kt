package co.id.footballmania.ui.detailleague

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import co.id.footballmania.utils.loadImageUrl
import co.id.footballmania.viewpager.PagerDetailAdapter
import kotlinx.android.synthetic.main.activity_detail_league.*
import co.id.footballmania.R
import co.id.footballmania.ui.detailleague.search.SearchActivity
import co.id.footballmania.ui.detailleague.search.SearchAdapter
import co.id.footballmania.ui.detailleague.search.SearchVM
import co.id.footballmania.utils.Constant
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.startActivity


class DetailLeague : AppCompatActivity() {

    private lateinit var searchVM: SearchVM
    private lateinit var detailLeagueVM: DetailLeagueVM
    private lateinit var adapter: SearchAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_league)
        viewpager_detail.adapter = PagerDetailAdapter(supportFragmentManager, intent.getStringExtra(Constant.ID_LEAGUE) as String)
        tabs_detail.setupWithViewPager(viewpager_detail)
        searchVM = ViewModelProviders.of(this).get(SearchVM::class.java)
        detailLeagueVM = ViewModelProviders.of(this).get(DetailLeagueVM::class.java)
        imgSearch.onClick {
            startActivity<SearchActivity>()
        }
        setData()
        getData()
    }

    private fun setData() {
        detailLeagueVM.setDetailLeague(intent.getStringExtra(Constant.ID_LEAGUE) as String)
    }

    private fun getData() {
        searchVM.getSearch().observe(this, Observer { searchItem ->
            adapter.setData(searchItem)
        })

        //detail
        detailLeagueVM.getDetailLeague().observe(this, Observer { league ->
            tvLeague.text = league?.get(0)?.strLeague
            tvDescription.text = league?.get(0)?.strDescriptionEN
            league?.get(0)?.strBadge?.let { ivClub.loadImageUrl(it) }
        })
    }

}
