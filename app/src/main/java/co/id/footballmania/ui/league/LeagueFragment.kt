package co.id.footballmania.ui.league

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import co.id.footballmania.R
import co.id.footballmania.model.listleague.LeagueItem
import co.id.footballmania.ui.detailleague.DetailLeague
import co.id.footballmania.utils.Constant
import kotlinx.android.synthetic.main.fragment_league.*

/**
 * A simple [Fragment] subclass.
 */
class LeagueFragment : Fragment() {

    private var items: MutableList<LeagueItem> = mutableListOf()
    private lateinit var adapter: LeagueAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_league, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()

        adapter = LeagueAdapter(items, object : LeagueAdapter.OnLeagueClickListener{
            override fun onClick(item: LeagueItem) {
                val intent = Intent(context, DetailLeague::class.java)
                intent.putExtra(Constant.ID_LEAGUE, item.id)
                startActivity(intent)
            }
        })
        adapter.notifyDataSetChanged()

        rv_league.layoutManager = LinearLayoutManager(context)
        rv_league.adapter = adapter
    }

    private fun initData(){
        val image = resources.getStringArray(R.array.league_logo)
        val name = resources.getStringArray(R.array.league_name)
        val id = resources.getStringArray(R.array.id_league)

        items.clear()

        for (i in name.indices){
            items.add(LeagueItem(id[i], name[i], image[i]))
        }

    }


}
