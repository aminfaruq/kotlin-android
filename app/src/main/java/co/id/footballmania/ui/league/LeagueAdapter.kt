package co.id.footballmania.ui.league

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.id.footballmania.R
import co.id.footballmania.model.listleague.LeagueItem
import co.id.footballmania.utils.loadImageUrl
import kotlinx.android.synthetic.main.item_league.view.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class LeagueAdapter(private val mData : List<LeagueItem> , val onClick : OnLeagueClickListener) : RecyclerView.Adapter<LeagueAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val mView = LayoutInflater.from(parent.context).inflate(R.layout.item_league,parent,false)
        return ViewHolder(mView, onClick)
    }

    override fun getItemCount(): Int = mData.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mData[position])
    }

    inner class ViewHolder(private val view: View, private val click : OnLeagueClickListener) : RecyclerView.ViewHolder(view) {
        fun bind(teamItem : LeagueItem ){
            with(itemView){
                tv_name.text = teamItem.leagueName
                teamItem.leagueImage?.let { imgLeague.loadImageUrl(it) }
            }
            view.onClick {
                click.onClick(teamItem)
            }

        }
    }

    interface OnLeagueClickListener{
        fun onClick(item : LeagueItem)
    }

}