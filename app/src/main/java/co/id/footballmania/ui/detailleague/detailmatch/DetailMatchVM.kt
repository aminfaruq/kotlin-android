package co.id.footballmania.ui.detailleague.detailmatch

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.id.footballmania.model.detailmatch.DetailMatch
import co.id.footballmania.model.listclub.TeamsItem
import co.id.footballmania.network.RetrofitApp
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailMatchVM : ViewModel() {
    private val teamDataHome = MutableLiveData<ArrayList<TeamsItem>>()
    private val teamDataAway = MutableLiveData<ArrayList<TeamsItem>>()
    private val detailMatch = MutableLiveData<ArrayList<DetailMatch>>()

    internal fun setDetailMatch(idEvent : String){
        val apiClient = RetrofitApp
            .create()
        apiClient.detailMatch(idEvent)
            .enqueue(object : Callback<DetailMatch.ResponseMatch>{
                override fun onResponse(
                    call: Call<DetailMatch.ResponseMatch>,
                    response: Response<DetailMatch.ResponseMatch>
                ) {
                    val response : DetailMatch.ResponseMatch = response.body()!!
                    detailMatch.value = response.events as ArrayList<DetailMatch>
                }

                override fun onFailure(call: Call<DetailMatch.ResponseMatch>, t: Throwable) {
                    Log.e("setDetailMatch", t.message.toString())
                }
            })
    }

    internal fun setDetailImage(idTeam : String, state : String){
        val apiClient = RetrofitApp
            .create()
        apiClient.lookupTeams(idTeam)
            .enqueue(object : Callback<TeamsItem.ResponseTeams>{
                override fun onResponse(
                    call: Call<TeamsItem.ResponseTeams>,
                    response: Response<TeamsItem.ResponseTeams>
                ) {
                    val response : TeamsItem.ResponseTeams = response.body()!!
                    if (state == "Home") {
                        teamDataHome.value = response.teams as ArrayList<TeamsItem>
                    } else {
                        teamDataAway.value = response.teams as ArrayList<TeamsItem>
                    }
                }

                override fun onFailure(call: Call<TeamsItem.ResponseTeams>, t: Throwable) {
                    Log.e("setDetailImage", t.message.toString())
                }
            })
    }

    internal fun getDetailTeamHome() : LiveData<ArrayList<TeamsItem>>{
        return teamDataHome
    }

    internal fun getDetailTeamAway() : LiveData<ArrayList<TeamsItem>>{
        return teamDataAway
    }

    internal fun getDetailMatch() : LiveData<ArrayList<DetailMatch>>{
        return detailMatch
    }

}