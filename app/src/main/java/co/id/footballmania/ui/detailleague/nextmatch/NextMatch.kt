package co.id.footballmania.ui.detailleague.nextmatch

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager

import co.id.footballmania.R
import co.id.footballmania.model.seach.EventItem
import co.id.footballmania.ui.detailleague.detailmatch.DetailMatchActivity
import co.id.footballmania.ui.detailleague.search.SearchAdapter
import co.id.footballmania.utils.Constant
import kotlinx.android.synthetic.main.next_match_fragment.*

class NextMatch : Fragment() {

    companion object {
        fun newInstance(idLeague : String) : NextMatch {
            val frag = NextMatch()
            val bundle = Bundle()
            bundle.putString(Constant.ID_LEAGUE, idLeague)
            frag.arguments = bundle
            return frag
        }
    }

    private lateinit var viewModel: NextMatchVM
    private val searchAdapter: SearchAdapter by lazy {
        SearchAdapter(object : SearchAdapter.OnItemClick{
            override fun onClick(item: EventItem) {
                //intent
                val intent = Intent(context, DetailMatchActivity::class.java)
                intent.putExtra(Constant.ID_EVENT, item.idEvent)
                intent.putExtra(Constant.ID_HOME_TEAMS, item.idHomeTeam)
                intent.putExtra(Constant.ID_AWAY_TEAMS, item.idAwayTeams)
                startActivity(intent)
            }
        })
    }
    private lateinit var idLeague : String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        idLeague = arguments?.getString(Constant.ID_LEAGUE).toString()
        return inflater.inflate(R.layout.next_match_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(NextMatchVM::class.java)
        // TODO: Use the ViewModel
        rv_next_match.adapter = searchAdapter
        rv_next_match.layoutManager = LinearLayoutManager(context)
        viewModel.setNextMatch(idLeague)
        viewModel.getNextMatch().observe(this, Observer { nextMatchItem ->
            if (nextMatchItem != null){
                searchAdapter.setData(nextMatchItem)
            }
        })
    }

}
