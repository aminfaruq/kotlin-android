package co.id.footballmania.ui.detailleague.favoritematch

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.id.footballmania.R
import co.id.footballmania.database.Favorite
import co.id.footballmania.utils.DateTime
import kotlinx.android.synthetic.main.item_detail.view.*

class FavoriteMatchAdapter(
    private val favorites: List<Favorite>,
    private val listener: (Favorite) -> Unit
) : RecyclerView.Adapter<FavoriteMatchAdapter.FavoriteViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        val mView = LayoutInflater.from(parent.context).inflate(R.layout.item_detail, parent, false)
        return FavoriteViewHolder(mView)
    }

    override fun getItemCount(): Int = favorites.size

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        holder.bindItem(favorites[position], listener)
    }

    class FavoriteViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindItem(favorite: Favorite, listener: (Favorite) -> Unit) {
            with(itemView){
                tv_date.text = DateTime.getLongDate(favorite.dateEvent ?: "")
                tv_score_home.text = favorite.intHomeScore
                tv_score_away.text = favorite.intAwayScore
                imgMatch1.text = favorite.strHomeTeam
                imgMatch2.text = favorite.strAwayTeam
            }
            itemView.setOnClickListener { listener(favorite) }
        }
    }
}
