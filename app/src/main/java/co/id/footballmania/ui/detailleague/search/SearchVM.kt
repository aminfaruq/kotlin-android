package co.id.footballmania.ui.detailleague.search

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.id.footballmania.model.seach.EventItem
import co.id.footballmania.network.RetrofitApp
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchVM : ViewModel() {
    private val listTeams = MutableLiveData<ArrayList<EventItem>>()

    internal fun setSearch(search : String){
        val apiClient = RetrofitApp
            .create()
        apiClient.searchEvent(search)
            .enqueue(object : Callback<EventItem.ResponseSearch>{
                override fun onResponse(
                    call: Call<EventItem.ResponseSearch>,
                    response: Response<EventItem.ResponseSearch>
                ) {
                    val result = arrayListOf<EventItem>()
                    if (response.body() != null){
                        val response : EventItem.ResponseSearch = response.body()!!
                        response.event?.forEach {
                            if (it.strSport?.equals("Soccer") == true){
                                result.add(it)
                            }
                        }
                        listTeams.value = result
                    }
                }

                override fun onFailure(call: Call<EventItem.ResponseSearch>, t: Throwable) {
                    Log.e("Error on detail vm" , t.message.toString())
                }
            })
    }

    internal fun getSearch() : LiveData<ArrayList<EventItem>>{
        return listTeams
    }
}