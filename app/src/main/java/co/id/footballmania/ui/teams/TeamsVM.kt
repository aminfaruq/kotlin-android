package co.id.footballmania.ui.teams

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.id.footballmania.model.listclub.TeamsItem
import co.id.footballmania.network.RetrofitApp
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@Suppress("NAME_SHADOWING")
class TeamsVM : ViewModel() {
    private val listTeams = MutableLiveData<ArrayList<TeamsItem>>()

    internal fun setTeams(league : String){
        val apiClient = RetrofitApp
            .create()

            apiClient.listClub(league)
            .enqueue(object : Callback<TeamsItem.ResponseTeams>{

                override fun onResponse(
                    call: Call<TeamsItem.ResponseTeams>,
                    response: Response<TeamsItem.ResponseTeams>
                ) {
                    if (response.body() != null){
                        val response : TeamsItem.ResponseTeams = response.body()!!
                        listTeams.value = response.teams as ArrayList<TeamsItem>?
                    }
                }
                override fun onFailure(call: Call<TeamsItem.ResponseTeams>, t: Throwable) {
                    Log.e("setTeams" , t.message.toString())
                }
            })
    }

    internal fun getTeams() : LiveData<ArrayList<TeamsItem>>{
        return listTeams
    }
}