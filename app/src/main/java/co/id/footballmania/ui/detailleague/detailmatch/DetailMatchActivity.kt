package co.id.footballmania.ui.detailleague.detailmatch

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import co.id.footballmania.R
import co.id.footballmania.database.Favorite
import co.id.footballmania.database.database
import co.id.footballmania.model.detailmatch.DetailMatch
import co.id.footballmania.utils.Constant
import co.id.footballmania.utils.loadImageUrl
import kotlinx.android.synthetic.main.activity_detail_match.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.toast

class DetailMatchActivity : AppCompatActivity() {
    private lateinit var detailMatchVM: DetailMatchVM
    private lateinit var idTeamHome : String
    private lateinit var idTeamAway : String
    private lateinit var idEvent : String
    private lateinit var detailMatch : DetailMatch
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_match)
        setSupportActionBar(toolbar)
        title = ""
        detailMatchVM = ViewModelProviders.of(this).get(DetailMatchVM::class.java)
        idTeamHome = intent.getStringExtra(Constant.ID_HOME_TEAMS) as String
        idTeamAway = intent.getStringExtra(Constant.ID_AWAY_TEAMS) as String
        idEvent = intent.getStringExtra(Constant.ID_EVENT) as String
        setData()
        getData()
    }

    private fun setData() {
        detailMatchVM.setDetailImage(idTeamHome, "Home")
        detailMatchVM.setDetailImage(idTeamAway, "Away")
        detailMatchVM.setDetailMatch(idEvent)
        favoriteState()
    }
    private fun getData() {
        //home
        detailMatchVM.getDetailTeamHome().observe(this, Observer { teamItem ->
            teamItem[0].strTeamBadge?.let { imgDetailHome.loadImageUrl(it) }
            teamItem[0].strTeamBadge?.let { imgDetailHome2.loadImageUrl(it) }
        })

        //away
        detailMatchVM.getDetailTeamAway().observe(this, Observer { teamItem ->
            teamItem[0].strTeamBadge?.let { imgDetailAway.loadImageUrl(it) }
            teamItem[0].strTeamBadge?.let { imgDetailAway2.loadImageUrl(it) }
        })

        //detail match
        detailMatchVM.getDetailMatch().observe(this, Observer { teamItem ->
            tv_event_name.text = teamItem[0].strEvent
            tv_name_home.text = teamItem[0].strHomeTeam
            tv_name_away.text = teamItem[0].strAwayTeam
            tv_shoot_home.text = teamItem[0].intHomeShots
            tv_shoot_away.text = teamItem[0].intAwayShots
            tv_yellow_home.text = teamItem[0].strHomeYellowCards
            tv_yellow_away.text = teamItem[0].strAwayYellowCards
            tv_red_home.text = teamItem[0].strHomeRedCards
            tv_red_away.text = teamItem[0].strAwayRedCards
            tv_formation_home.text = teamItem[0].strHomeFormation
            tv_formation_away.text = teamItem[0].strAwayFormation
            tv_detail_score_home.text = teamItem[0].intHomeScore
            tv_detail_score_away.text = teamItem[0].intAwayScore
            detailMatch = teamItem[0]
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.add_to_favorite -> {
                if (isFavorite) removeFromFavorite()
                else addToFavorite()
                isFavorite = !isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun favoriteState() {
        database.use {
            val result = select(Favorite.TABLE_FAVORITE)
                .whereArgs(
                    Favorite.EVENT_ID + " = {eventId}",
                    "eventId" to idEvent
                )
            val favorite = result.parseList(classParser<Favorite>())
            if (favorite.isNotEmpty()) isFavorite = true
        }
    }


    private fun addToFavorite() {
        try {
            database.use {
                insert(
                    Favorite.TABLE_FAVORITE,
                    Favorite.EVENT_ID to detailMatch.idEvent,
                    Favorite.HOME_ID to detailMatch.idHomeTeam,
                    Favorite.AWAY_ID to detailMatch.idAwayTeam,
                    Favorite.STR_EVENT to detailMatch.strEvent,
                    Favorite.STR_BADGE_HOME to detailMatch.strTeamBadge,
                    Favorite.STR_BADGE_AWAY to detailMatch.strTeamBadge,
                    Favorite.STR_HOME_TEAM to detailMatch.strHomeTeam,
                    Favorite.STR_AWAY_TEAM to detailMatch.strAwayTeam,
                    Favorite.INT_HOME_SHOTS to detailMatch.intHomeShots,
                    Favorite.INT_AWAY_SHOTS to detailMatch.intAwayShots,
                    Favorite.STR_HOME_YELLOW_CARDS to detailMatch.strHomeYellowCards,
                    Favorite.STR_AWAY_YELLOW_CARDS to detailMatch.strAwayYellowCards,
                    Favorite.STR_HOME_RED_CARDS to detailMatch.strHomeRedCards,
                    Favorite.STR_AWAY_RED_CARDS to detailMatch.strAwayRedCards,
                    Favorite.STR_HOME_FORMATION to detailMatch.strHomeFormation,
                    Favorite.STR_AWAY_FORMATION to detailMatch.strAwayFormation,
                    Favorite.INT_HOME_SCORE to detailMatch.intHomeScore,
                    Favorite.INT_AWAY_SCORE to detailMatch.intAwayScore
                )
            }
            toast("Data disimpan")
            setFavorite()
        } catch (e: SQLiteConstraintException) {
        }
    }

    private fun removeFromFavorite() {
        try {
            database.use {
                delete(
                    Favorite.TABLE_FAVORITE,
                    Favorite.EVENT_ID + " = {eventId}",
                    "eventId" to idEvent
                )
            }
            toast("Data dihapus")
            setFavorite()
        } catch (e: SQLiteConstraintException) {
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_added_to_favorite)
        else
            menuItem?.getItem(0)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_add_to_favorite)
    }


}

