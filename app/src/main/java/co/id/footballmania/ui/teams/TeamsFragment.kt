package co.id.footballmania.ui.teams


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager

import co.id.footballmania.R
import kotlinx.android.synthetic.main.fragment_teams.*

/**
 * A simple [Fragment] subclass.
 */
class TeamsFragment : Fragment() {

    private lateinit var adapter: TeamsAdapter
    private lateinit var mainVM: TeamsVM
    private lateinit var league : String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_teams, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = TeamsAdapter()
        adapter.notifyDataSetChanged()

        rv_teams.layoutManager = LinearLayoutManager(context)
        rv_teams.adapter = adapter

        mainVM = ViewModelProviders.of(this).get(TeamsVM::class.java)
        val spinnerItem  = resources.getStringArray(R.array.league_name)
        val spinnerAdapter = context?.let {
            ArrayAdapter(
                it,
                android.R.layout.simple_spinner_dropdown_item,
                spinnerItem
            )
        }
        spinner.adapter = spinnerAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                league = spinner.selectedItem.toString()
                mainVM.setTeams(league)
                showLoading(true)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }

        mainVM.getTeams().observe(this, Observer { teamsItem ->
            if (teamsItem != null){
                adapter.setData(teamsItem)
                showLoading(false)
            }
        })
    }

     fun showLoading(state : Boolean){
        if(state){
            progressBar.visibility = View.VISIBLE
        }else{
            progressBar.visibility = View.GONE
        }
    }


}
