package co.id.footballmania.ui.detailleague

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.id.footballmania.model.detailleague.DetailLeagueModel
import co.id.footballmania.network.RetrofitApp
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailLeagueVM : ViewModel() {
    private val detailLeague = MutableLiveData<ArrayList<DetailLeagueModel>>()

    internal fun setDetailLeague(idLeague : String){
        val apiClient = RetrofitApp
            .create()
        apiClient.detailLeague(idLeague)
            .enqueue(object : Callback<DetailLeagueModel.ResponseDetailLeague>{

                override fun onResponse(
                    call: Call<DetailLeagueModel.ResponseDetailLeague>,
                    response: Response<DetailLeagueModel.ResponseDetailLeague>
                ) {
                    val response : DetailLeagueModel.ResponseDetailLeague = response.body()!!
                    detailLeague.value = response.leagues as ArrayList<DetailLeagueModel>
                }

                override fun onFailure(
                    call: Call<DetailLeagueModel.ResponseDetailLeague>,
                    t: Throwable
                ) {
                    Log.e("setDetailLeague", t.message.toString())
                }
            })
    }

    internal fun getDetailLeague() : LiveData<ArrayList<DetailLeagueModel>>{
        return detailLeague
    }
}