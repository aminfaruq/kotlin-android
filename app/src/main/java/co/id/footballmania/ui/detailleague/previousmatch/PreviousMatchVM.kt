package co.id.footballmania.ui.detailleague.previousmatch

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import co.id.footballmania.model.seach.EventItem
import co.id.footballmania.network.RetrofitApp
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@Suppress("NAME_SHADOWING")
class PreviousMatchVM : ViewModel() {
    // TODO: Implement the ViewModel
    private val listPreviousMatch = MutableLiveData<ArrayList<EventItem>>()
    internal fun setNextMatch(id : String){
        val apiClient = RetrofitApp
            .create()
        apiClient.previousMatch(id)
            .enqueue(object : Callback<EventItem.ResponseSearch>{
                override fun onResponse(
                    call: Call<EventItem.ResponseSearch>,
                    response: Response<EventItem.ResponseSearch>
                ) {
                    val response = response.body()
                    listPreviousMatch.value = response?.events as ArrayList<EventItem>?
                }

                override fun onFailure(call: Call<EventItem.ResponseSearch>, t: Throwable) {
                    Log.e("Error in NextMatch" , t.message.toString())
                }
            })
    }

    internal fun getNextMatch() : LiveData<ArrayList<EventItem>> {
        return listPreviousMatch
    }

}
