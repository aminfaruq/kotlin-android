package co.id.footballmania.ui.detailleague.search

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import co.id.footballmania.R
import co.id.footballmania.model.seach.EventItem
import co.id.footballmania.ui.detailleague.detailmatch.DetailMatchActivity
import co.id.footballmania.utils.Constant
import kotlinx.android.synthetic.main.activity_search.*

class SearchActivity : AppCompatActivity() {
    private lateinit var adapter: SearchAdapter
    private lateinit var searchVM: SearchVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        adapter = SearchAdapter(object : SearchAdapter.OnItemClick{
            override fun onClick(item: EventItem) {
                //intent
                val intent = Intent(this@SearchActivity, DetailMatchActivity::class.java)
                intent.putExtra(Constant.ID_EVENT, item.idEvent)
                intent.putExtra(Constant.ID_HOME_TEAMS, item.idHomeTeam)
                intent.putExtra(Constant.ID_AWAY_TEAMS, item.idAwayTeams)
                startActivity(intent)
            }
        })
        rv_search.adapter = adapter
        rv_search.layoutManager = LinearLayoutManager(this)
        searchQuery()
        searchVM = ViewModelProviders.of(this).get(SearchVM::class.java)

    }

    private fun searchQuery() {
        searchMatch.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchVM.setSearch(query?:"")
                observeData()
                showLoading(true)
                return true
            }
            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
    }

    private fun observeData() {
        searchVM.getSearch().observe(this, Observer { searchItem ->
            if (searchItem != null){
                adapter.setData(searchItem)
                showLoading(false)
            }
        })
    }

    private fun showLoading(state : Boolean){
        if(state){
            progressBarSearch.visibility = View.VISIBLE
        }else{
            progressBarSearch.visibility = View.GONE
        }
    }

}
